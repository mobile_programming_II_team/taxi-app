package com.amp.pickup.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amp.pickup.data.model.Taxi

@Dao
interface TaxiDao {

    @Query("SELECT * FROM taxis")
    fun getAll(): LiveData<List<Taxi>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Taxi>)

    @Delete
    fun deleteAll(users: List<Taxi>)

    @Query("DELETE FROM taxis")
    fun deleteAll()

    @Query("select * from taxis where id = :id")
    fun findTaxiById(id: String): LiveData<Taxi>

    @Query("select * from taxis where driverId = :userId")
    fun findTaxiByUserId(userId: String): LiveData<Taxi>

    @Query("select * from taxis")
    fun findAllTaxis(): LiveData<List<Taxi>>

    @Query("select * from taxis where vehicleType = :vehicleType")
    fun findTaxiByVehicleType(vehicleType: String): LiveData<List<Taxi>>

    @Query("select * from taxis where vehicleGrade = :vehicleGrade")
    fun findTaxiByVehicleGrade(vehicleGrade: String): LiveData<List<Taxi>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTaxi(taxi: Taxi)

    @Update
    fun updateTaxi(taxi: Taxi)

    @Delete
    fun deleteTaxi(taxi: Taxi)
}