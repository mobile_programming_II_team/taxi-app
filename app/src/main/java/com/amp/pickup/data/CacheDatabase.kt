package com.amp.pickup.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.amp.pickup.data.dao.*
import com.amp.pickup.data.model.*

@Database(
    entities = arrayOf(Fare::class, Feedback::class, Location::class, Taxi::class, User::class),
    version = 1,
    exportSchema = false
)
abstract class CacheDatabase : RoomDatabase() {

    abstract fun fareDao(): FareDao
    abstract fun feedbackDao(): FeedbackDao
    abstract fun locationDao(): LocationDao
    abstract fun taxiDao(): TaxiDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var INSTANCE: CacheDatabase? = null

        fun getDatabase(context: Context): CacheDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CacheDatabase::class.java, "cache_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }


    }


}