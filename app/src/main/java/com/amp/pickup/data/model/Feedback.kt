package com.amp.pickup.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feedbacks")
data class Feedback(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "rating") val rating: Int,
    @ColumnInfo(name = "review") val review: String

)