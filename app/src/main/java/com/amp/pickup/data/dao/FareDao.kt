package com.amp.pickup.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amp.pickup.data.model.Fare

@Dao
interface FareDao {

    @Query("SELECT * FROM fares")
    fun getAll(): LiveData<List<Fare>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Fare>)

    @Delete
    fun deleteAll(users: List<Fare>)

    @Query("DELETE FROM fares")
    fun deleteAll()

    @Query("select * from fares where passengerId = :passengerId")
    fun findFaresByPassengerId(passengerId: String): LiveData<List<Fare>>

    @Query("select * from fares where taxiId = :taxiId")
    fun findFaresByTaxiId(taxiId: String): LiveData<List<Fare>>

    @Query("select * from fares where id = :id")
    fun findFaresById(id: String): LiveData<Fare>

    @Query("select * from fares")
    fun findAllFares(): LiveData<List<Fare>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFare(fare: Fare)

    @Update
    fun updateFare(fare: Fare)

    @Delete
    fun deleteFare(fare: Fare)


}