package com.amp.pickup.data.api

import com.amp.pickup.data.model.Fare
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface FareApiService {
    //handle requests involving "fare"
    @GET("fare/all")
    fun findAllFaresAsync(@Header("Authorization") authHeader: String): Deferred<Response<List<Fare>>>

    @GET("fare/{id}")
    fun findFareByIdAsync(@Header("Authorization") authHeader: String, @Path("id") id: String): Deferred<Response<Fare>>

    @GET("fare/current")
    fun findFareByIdAsync(@Header("Authorization") authHeader: String): Deferred<Response<Fare>>

    @POST("fare/request")
    fun requestFareAsync(
        @Header("Authorization") authHeader: String,
        @Body path: com.amp.pickup.data.model.Path,
        @Query("taxi_id") taxiId: String
    ): Deferred<Response<Fare>>

    @PUT("fare/change-status")
    fun changeStatusAsync(@Header("Authorization") authHeader: String, @Query("fare_id") fareId: String, @Query("status") status: String): Deferred<Response<Fare>>

    @PUT("fare/edit")
    fun updateFareAsync(@Header("Authorization") authHeader: String, @Body fare: Fare): Deferred<Response<Fare>>

    @DELETE("fare")
    fun deleteFareAsync(@Header("Authorization") authHeader: String, @Body fare: Fare): Deferred<Response<Fare>>

//    @PUT("fare/{id}")
//    fun updateFareAsync(@Path("id") id: UUID, @Body newFare: Fare): Deferred<Response<Void>>
//
//    @DELETE("fare/{id}")
//    fun deleteFareAsync(@Path ("id")id:UUID):Deferred<Response<Void>>


//
//    //handle requests involving "location"
//    @GET("location/{id}")
//    fun findLocationByIdAsync(@Path("id") id:UUID): Deferred<Response<Location>>
//
//    @POST("location")
//    fun insertLocationAasync(@Body newLocation: Location): Deferred<Response<Location>>
//
//    @PUT("location/{id}")
//    fun updateLocationAsync(@Path("id") id: UUID, @Body newLocation: Location): Deferred<Response<Void>>
//
//    @DELETE("location/{id}")
//    fun deleteLocationAsync(@Path("id")id:UUID): Deferred<Response<Void>>


    companion object {
        private const val baseUrl = BASE_URL
        fun getInstance(): FareApiService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(FareApiService::class.java)

        }

    }
}