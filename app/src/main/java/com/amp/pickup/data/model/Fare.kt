package com.amp.pickup.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "fares")

data class Fare(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "taxiId") val taxiId: String,
    @ColumnInfo(name = "passengerId") val passengerId: String,
    @ColumnInfo(name = "startPointId") val startPointId: String,
    @ColumnInfo(name = "destinationId") val destinationId: String,
    @ColumnInfo(name = "status") val status: String = "REQUESTED"
)
