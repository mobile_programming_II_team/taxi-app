package com.amp.pickup.data.api

import com.amp.pickup.data.model.Feedback
import com.amp.pickup.data.model.Location
import com.amp.pickup.data.model.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface UserApiService {

    //handle requests involving "user"
//    @GET("user/{email}")
//    fun findAllUserAsync(@Path("user") email:String): Deferred<Response<User>>
    @GET("user/self")
    fun findLoggedInUserAsync(@Header("Authorization") authHeader: String): Deferred<Response<User>>

    @GET("user/{id}")
    fun findUserByIdAsync(@Header("Authorization") authHeader: String, @Path("id") id: String): Deferred<Response<User>>

    @GET("user/all")
    fun findAllUsers(): Deferred<Response<List<User>>>

    @GET("user/feedbacks")
    fun findUserFeedbacksAsync(@Header("Authorization") authHeader: String, @Query("user_id") userId: String): Deferred<Response<List<Feedback>>>

    @POST("user/new")
    fun insertUserAasync(@Body newUser: User): Deferred<Response<User>>

    @PUT("user/request-upgrade")
    fun requestToBeDriverAsync(
        @Header("Authorization") authHeader: String, @Query("vehicle_type") vehicleType: String, @Query(
            "vehicle_grade"
        ) vehicleGrade: String
    ): Deferred<Response<User>>

    @PUT("user/approve-upgrade")
    fun approvedDriverAsync(@Header("Authorization") authHeader: String): Deferred<Response<User>>

    @PUT("user/deny-upgrade")
    fun denyedDriverAsync(@Header("Authorization") authHeader: String): Deferred<Response<User>>

    @PUT("user/add-feedback")
    fun addFeedbackAsync(@Header("Authorization") authHeader: String, @Query("user_id") userId: String, @Body newFeedback: Feedback): Deferred<Response<User>>

    @PUT("user/update-location")
    fun updateLocationAsync(@Header("Authorization") authHeader: String, @Body newLocation: Location): Deferred<Response<User>>

    @PUT("user/edit-account")
    fun editAccountAsync(@Header("Authorization") authHeader: String, @Body user: User): Deferred<Response<User>>

    @DELETE("user")
    fun deleteAccountAsync(@Header("Authorization") authHeader: String, @Body user: User): Deferred<Response<User>>

//    @DELETE("user/{id}")
//    fun deleteUserAsync(@Path("id")id: String): Deferred<Response<Void>>

    companion object {
        private const val baseUrl = BASE_URL
        fun getInstance(): UserApiService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(UserApiService::class.java)

        }

    }

}