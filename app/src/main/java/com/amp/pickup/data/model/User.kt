package com.amp.pickup.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @PrimaryKey @ColumnInfo(name = "id") var id: String,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = "password") val password: String?,
    @ColumnInfo(name = "phoneNo") val phoneNo: String? = null,
    @ColumnInfo(name = "driverStatus") val driverStatus: String? = "NOT_DRIVER",
    @ColumnInfo(name = "locationId") val locationId: String?,
    @ColumnInfo(name = "role") val role: String?
)