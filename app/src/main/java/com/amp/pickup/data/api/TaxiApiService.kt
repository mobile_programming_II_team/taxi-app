package com.amp.pickup.data.api

import com.amp.pickup.data.model.Taxi
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface TaxiApiService {

    //handle requests involving "taxi"
/*    @GET("taxi/{userId}")
    fun findAllTaxiAsync(@Path("userId") userId: String): Deferred<Response<Taxi>>
*/
    @GET("taxi/{id}")
    fun findTaxiByIdAsync(@Header("Authorization") authHeader: String, @Path("id") id: String): Deferred<Response<Taxi>>

    @GET("taxi/all")
    fun findAllTaxisAsync(@Header("Authorization") authHeader: String): Deferred<Response<List<Taxi>>>

    @GET("taxi/by-vehicle-grade")
    fun findTaxiByGradeAsync(@Header("Authorization") authHeader: String, @Query("vehicle_grade") vehicleGrade: String): Deferred<Response<List<Taxi>>>

    @GET("taxi/by-vehicle-type")
    fun findTaxiByTypeAsync(@Header("Authorization") authHeader: String, @Query("vehicle_type") vehicleType: String): Deferred<Response<List<Taxi>>>

    @POST("taxi/new")
    fun insertTaxiAsync(@Header("Authorization") authHeader: String, @Body newTaxi: Taxi): Deferred<Response<Taxi>>

    @PUT("taxi/edit")
    fun updateTaxiAsync(@Header("Authorization") authHeader: String, @Body taxi: Taxi): Deferred<Response<Taxi>>

    @DELETE("taxi")
    fun deleteTaxiAsync(@Header("Authorization") authHeader: String, @Body taxi: Taxi): Deferred<Response<Taxi>>

    /*
        @PUT("taxi/{id}")
        fun updateTaxiAsync(@Path("id") id: String, @Body newTaxi: Taxi): Deferred<Response<Void>>

        @DELETE("taxi/{id}")
        fun deleteTaxiAsync(@Path("id")id: String): Deferred<Response<Void>>
    */
    companion object {
        private const val baseUrl = BASE_URL
        fun getInstance(): TaxiApiService {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
            return retrofit.create(TaxiApiService::class.java)

        }

    }
}