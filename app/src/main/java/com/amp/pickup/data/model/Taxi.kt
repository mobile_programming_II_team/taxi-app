package com.amp.pickup.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "taxis")
data class Taxi(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "vehicleType") val vehicleType: String,
    @ColumnInfo(name = "vehicleGrade") val vehicleGrade: String = "AVERAGE",
    @ColumnInfo(name = "driverId") val driverId: String

)