package com.amp.pickup.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amp.pickup.data.model.User

@Dao
interface UserDao {

    @Query("SELECT * FROM users")
    fun getAll(): LiveData<List<User>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<User>)

    @Delete
    fun deleteAll(users: List<User>)

    @Query("DELETE FROM users")
    fun deleteAll()

    @Query("select * from users where id = :id")
    fun findUserById(id: String): LiveData<User>

    @Query("select * from users where email = :email")
    fun findUserByEmail(email: String): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Update
    fun updateUser(user: User)

    @Delete
    fun deleteUser(user: User)
}