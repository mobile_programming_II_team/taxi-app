package com.amp.pickup.data.model

data class Path(val startingPoint: Location, val destination: Location)