package com.amp.pickup.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amp.pickup.data.model.Feedback

@Dao
interface FeedbackDao {

    @Query("SELECT * FROM feedbacks")
    fun getAll(): LiveData<List<Feedback>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Feedback>)

    @Delete
    fun deleteAll(users: List<Feedback>)

    @Query("DELETE FROM feedbacks")
    fun deleteAll()

    @Query("select * from feedbacks where userId = :userId")
    fun findFeedbacksByUserId(userId: String): LiveData<List<Feedback>>

    @Query("select * from feedbacks where id = :id")
    fun findFeedbackById(id: String): LiveData<Feedback>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFeedback(feedback: Feedback)

    @Update
    fun updateFeedback(feedback: Feedback)

    @Delete
    fun deleteFeedback(feedback: Feedback)


}