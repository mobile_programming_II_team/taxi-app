package com.amp.pickup.data.util

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("app:showIf")
fun showIf(view: View, hasError: Boolean) {
    view.visibility = if (hasError) View.VISIBLE else View.GONE
}