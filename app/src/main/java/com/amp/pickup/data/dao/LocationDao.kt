package com.amp.pickup.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.amp.pickup.data.model.Location

@Dao
interface LocationDao {

    @Query("SELECT * FROM locations")
    fun getAll(): LiveData<List<Location>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<Location>)

    @Delete
    fun deleteAll(users: List<Location>)

    @Query("DELETE FROM locations")
    fun deleteAll()

    @Query("select * from locations where id = :id")
    fun findLocationById(id: String): LiveData<Location>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocation(location: Location)

    @Update
    fun updateLocation(location: Location)

    @Delete
    fun deleteLocation(location: Location)
}