package com.amp.pickup.data.api

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

const val BASE_URL = "http://192.168.43.15:7990/"

class CheckConnection {
    companion object {
        fun check(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

            return networkInfo != null && networkInfo.isConnected
        }
    }
}