package com.amp.pickup.repository

import android.app.Application
import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amp.pickup.R
import com.amp.pickup.data.api.CheckConnection
import com.amp.pickup.data.api.UserApiService
import com.amp.pickup.data.dao.FeedbackDao
import com.amp.pickup.data.dao.UserDao
import com.amp.pickup.data.model.Feedback
import com.amp.pickup.data.model.Location
import com.amp.pickup.data.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UserRepository(private val userDao: UserDao, var application: Application, private val feedbackDao: FeedbackDao) {

    private lateinit var userApiService: UserApiService
    private val sharedPreference =
        application.getSharedPreferences(application.resources.getString(R.string.shared_pref), Context.MODE_PRIVATE)
    private lateinit var authHeader: String
    private lateinit var loggedInEmail: String

    init {
        if (CheckConnection.check(application)) {
            userApiService = UserApiService.getInstance()
        }
        if (sharedPreference != null) {
            authHeader =
                sharedPreference.getString(application.resources.getString(R.string.auth_header), "Auth String")
            loggedInEmail =
                sharedPreference.getString(application.resources.getString(R.string.logged_in_email), "Email")
        }
        GlobalScope.launch { updateUsersCache() }
    }

    @WorkerThread
    private suspend fun updateUsersCache() {
        if (CheckConnection.check(application)) {
            val users = userApiService.findAllUsers().await().body()
            if (users != null && users.isNotEmpty()) {
                userDao.deleteAll()
                userDao.insertAll(users)
            }
        }
    }

    @WorkerThread
    suspend fun findLoggedInUser(): LiveData<User> {
        authHeader = sharedPreference.getString(application.resources.getString(R.string.auth_header), "Auth String")
        loggedInEmail = sharedPreference.getString(application.resources.getString(R.string.logged_in_email), "Email")
        var user: LiveData<User> = MutableLiveData()
        if (CheckConnection.check(application)) {
            if (userApiService.findLoggedInUserAsync(authHeader).await().body() != null) {
                user = userDao.findUserByEmail(loggedInEmail)
            }
        }
        print("at the end : ")
        println(user.value)
        return user
    }

    @WorkerThread
    fun findUserById(id: String): LiveData<User> {
        return userDao.findUserById(id)
    }

    @WorkerThread
    suspend fun insertUser(newUser: User): LiveData<User> {
        if (CheckConnection.check(application)) {
            userApiService.insertUserAasync(newUser).await()
            updateUsersCache()
        }
        return userDao.findUserById(newUser.id)
    }

    @WorkerThread
    fun findUserFeedbacks(userId: String): LiveData<List<Feedback>> {
        return feedbackDao.findFeedbacksByUserId(userId)
    }

    @WorkerThread
    suspend fun requestToBeDriver(vehicleType: String, vehicleGrade: String) {
        if (CheckConnection.check(application)) {
            userApiService.requestToBeDriverAsync(authHeader, vehicleType, vehicleGrade).await().body()
            updateUsersCache()
        }
    }

    @WorkerThread
    suspend fun approveDriver() {
        if (CheckConnection.check(application)) {
            userApiService.approvedDriverAsync(authHeader).await()
            updateUsersCache()
        }
    }

    @WorkerThread
    suspend fun deniedDriver() {
        if (CheckConnection.check(application)) {
            userApiService.denyedDriverAsync(authHeader).await()
            updateUsersCache()
        }
    }

    @WorkerThread
    suspend fun addFeedback(userId: String, newFeedback: Feedback) {
        if (CheckConnection.check(application)) {
            userApiService.addFeedbackAsync(authHeader, userId, newFeedback).await()
            updateUsersCache()
        }
    }

    @WorkerThread
    suspend fun updateLocation(newLocation: Location) {
        if (CheckConnection.check(application)) {
            userApiService.updateLocationAsync(authHeader, newLocation).await()
            updateUsersCache()
        }
    }

    @WorkerThread
    suspend fun updateUser(user: User) {
        userApiService.editAccountAsync(authHeader, user).await()
        updateUsersCache()
    }

    @WorkerThread
    suspend fun deleteUser(user: User) {
        userApiService.deleteAccountAsync(authHeader, user).await()
        updateUsersCache()
    }

}