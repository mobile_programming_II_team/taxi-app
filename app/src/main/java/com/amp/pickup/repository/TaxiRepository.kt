package com.amp.pickup.repository

import android.app.Application
import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.amp.pickup.R
import com.amp.pickup.data.api.CheckConnection
import com.amp.pickup.data.api.TaxiApiService
import com.amp.pickup.data.dao.TaxiDao
import com.amp.pickup.data.model.Taxi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TaxiRepository(private val taxiDao: TaxiDao, var application: Application) {

    private lateinit var taxiApiService: TaxiApiService
    private val sharedPreference =
        application.getSharedPreferences(application.resources.getString(R.string.shared_pref), Context.MODE_PRIVATE)
    private lateinit var authHeader: String

    init {
        if (CheckConnection.check(application)) {
            taxiApiService = TaxiApiService.getInstance()
        }
        if (sharedPreference != null) authHeader =
            sharedPreference.getString(application.resources.getString(R.string.auth_header), "")
        GlobalScope.launch { updateTaxisCache() }
    }

    @WorkerThread
    private suspend fun updateTaxisCache() {
        val taxis: List<Taxi>?
        if (CheckConnection.check(application)) {
            taxis = taxiApiService.findAllTaxisAsync(authHeader).await().body()
            if (taxis != null && taxis.isNotEmpty()) {
                taxiDao.deleteAll()
                taxiDao.insertAll(taxis)
            }
        }
    }

    @WorkerThread
    fun findTaxiById(id: String): LiveData<Taxi> {
        return taxiDao.findTaxiById(id)
    }

    @WorkerThread
    fun findAllTaxis(): LiveData<List<Taxi>> {
        return taxiDao.findAllTaxis()
    }

    @WorkerThread
    fun findTaxiByGrade(vehicleGrade: String): LiveData<List<Taxi>> {
        return taxiDao.findTaxiByVehicleGrade(vehicleGrade)
    }

    @WorkerThread
    fun findTaxiByType(vehicleType: String): LiveData<List<Taxi>> {
        return taxiDao.findTaxiByVehicleType(vehicleType)
    }

    @WorkerThread
    private suspend fun insertTaxi(taxi: Taxi) {
        if (CheckConnection.check(application)) {
            taxiApiService.insertTaxiAsync(authHeader, taxi).await()
            updateTaxisCache()
        }
    }

    @WorkerThread
    suspend fun updateTaxi(taxi: Taxi) {
        if (CheckConnection.check(application)) {
            taxiApiService.updateTaxiAsync(authHeader, taxi).await()
            updateTaxisCache()
        }
    }

    @WorkerThread
    suspend fun deleteTaxi(taxi: Taxi) {
        if (CheckConnection.check(application)) {
            taxiApiService.deleteTaxiAsync(authHeader, taxi).await()
            updateTaxisCache()
        }
    }
}