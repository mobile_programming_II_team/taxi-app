package com.amp.pickup.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.amp.pickup.data.dao.FeedbackDao
import com.amp.pickup.data.model.Feedback

class FeedbackRepository(private val feedbackDao: FeedbackDao) {

    @WorkerThread
    fun findFeedbackByuserId(userId: String): LiveData<List<Feedback>> = feedbackDao.findFeedbacksByUserId(userId)

    @WorkerThread
    fun findFeedbackById(id: String): LiveData<Feedback> = feedbackDao.findFeedbackById(id)

    @WorkerThread
    fun insertFeedback(feedback: Feedback) {
        feedbackDao.insertFeedback(feedback)
    }

    @WorkerThread
    fun updateFeedBack(feedback: Feedback) {
        feedbackDao.updateFeedback(feedback)
    }

    @WorkerThread
    fun deleteFeedback(feedback: Feedback) {
        feedbackDao.deleteFeedback(feedback)
    }
}