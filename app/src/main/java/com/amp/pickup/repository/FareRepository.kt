package com.amp.pickup.repository

import android.app.Application
import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.amp.pickup.R
import com.amp.pickup.data.api.CheckConnection
import com.amp.pickup.data.api.FareApiService
import com.amp.pickup.data.dao.FareDao
import com.amp.pickup.data.model.Fare
import com.amp.pickup.data.model.Path
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FareRepository(private val fareDao: FareDao, var application: Application) {

    private lateinit var fareApiService: FareApiService
    private val sharedPreference =
        application.getSharedPreferences(application.resources.getString(R.string.shared_pref), Context.MODE_PRIVATE)
    private lateinit var authHeader: String


    init {
        if (CheckConnection.check(application)) {
            fareApiService = FareApiService.getInstance()
        }
        if (sharedPreference != null) authHeader =
            sharedPreference.getString(application.resources.getString(R.string.auth_header), "")
        GlobalScope.launch { updateFaresCache() }
    }

    @WorkerThread
    private suspend fun updateFaresCache() {
        val fares: List<Fare>?
        if (CheckConnection.check(application)) {
            fares = fareApiService.findAllFaresAsync(authHeader).await().body()
            if (fares != null && fares.isNotEmpty()) {
                fareDao.deleteAll()
                fareDao.insertAll(fares)
            }
        }
    }


    @WorkerThread
    fun findAllFares(): LiveData<List<Fare>> {
        return fareDao.findAllFares()
    }

    @WorkerThread
    fun findFareById(id: String): LiveData<Fare> {
        return fareDao.findFaresById(id)
    }

    @WorkerThread
    suspend fun requestFare(path: Path, taxiId: String) {
        if (CheckConnection.check(application)) {
            fareApiService.requestFareAsync(authHeader, path, taxiId).await()
            updateFaresCache()
        }
    }

    @WorkerThread
    suspend fun changeStatus(fareId: String, status: String) {
        if (CheckConnection.check(application)) {
            fareApiService.changeStatusAsync(authHeader, fareId, status).await()
            updateFaresCache()
        }
    }

    @WorkerThread
    suspend fun updateFare(fare: Fare) {
        if (CheckConnection.check(application)) {
            fareApiService.updateFareAsync(authHeader, fare).await()
            updateFaresCache()
        }
    }

    @WorkerThread
    suspend fun deleteFare(fare: Fare) {
        if (CheckConnection.check(application)) {
            fareApiService.deleteFareAsync(authHeader, fare).await()
            updateFaresCache()
        }
    }

}