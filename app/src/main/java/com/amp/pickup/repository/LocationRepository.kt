package com.amp.pickup.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.amp.pickup.data.dao.LocationDao
import com.amp.pickup.data.model.Location

class LocationRepository(private val locationDao: LocationDao) {

    @WorkerThread
    fun findLocationById(id: String): LiveData<Location> = locationDao.findLocationById(id)

    @WorkerThread
    fun insertLocation(location: Location) {
        locationDao.insertLocation(location)
    }

    @WorkerThread
    fun updateLocation(location: Location) {
        locationDao.updateLocation(location)
    }

    @WorkerThread
    fun deleteLocation(location: Location) {
        locationDao.deleteLocation(location)
    }
}