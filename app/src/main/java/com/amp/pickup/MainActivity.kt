package com.amp.pickup

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onSupportNavigateUp() =
        findNavController(this, R.id.my_nav_host_fragment).navigateUp()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = findNavController(this, R.id.my_nav_host_fragment)

        setupBottomNavigation(navController)
    }

    private fun setupBottomNavigation(navController: NavController) {
        NavigationUI.setupWithNavController(botNav, navController)
    }

//    private val onBottomNavigationIt = BottomNavigationView.OnNavigationItemSelectedListener {
//        when (it.itemId) {
//            R.id.loginFragment -> {
//                supportFragmentManager.beginTransaction()
//                    .replace(R.id.my_nav_host_fragment, LoginFragment()).addToBackStack(null).commit()
//                return@OnNavigationItemSelectedListener true
//            }
//            R.id.registerFragment -> {
//                supportFragmentManager.beginTransaction()
//                    .replace(R.id.my_nav_host_fragment, RegisterFragment()).addToBackStack(null).commit()
//                return@OnNavigationItemSelectedListener true
//            }
//        }
//        false
//    }
}
