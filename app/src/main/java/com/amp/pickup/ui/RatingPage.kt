package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentRatingPageBinding
import com.amp.pickup.viewmodel.FeedbackViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class RatingPage : Fragment() {
    lateinit var binding: FragmentRatingPageBinding
    private val feedbackViewModel by lazy { ViewModelProviders.of(this).get(FeedbackViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.viewmodel = feedbackViewModel
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_rating_page, container, false)
        return binding.root
    }


}
