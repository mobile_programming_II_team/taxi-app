package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentFarePageBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class FarePage : Fragment() {
    lateinit var binding: FragmentFarePageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_fare_page, container, false)
        return binding.root
    }


}
