package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentFaresListBinding
import com.amp.pickup.viewmodel.FaresListViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class FareList : Fragment() {
    lateinit var binding: FragmentFaresListBinding
    private val faresViewModel by lazy { ViewModelProviders.of(this).get(FaresListViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding.viewmodel = faresViewModel
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_fares_list, container, false)
        return binding.root
    }


}
