package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentRegisterBinding
import com.amp.pickup.viewmodel.RegisterViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding
    private val registerViewModel by lazy { ViewModelProviders.of(this).get(RegisterViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_register, container, false)
        binding.viewmodel = registerViewModel

        return binding.root
    }


}
