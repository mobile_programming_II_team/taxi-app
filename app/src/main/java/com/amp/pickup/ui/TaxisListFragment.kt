package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentTaxisListBinding
import com.amp.pickup.ui.recyclerView.adapter.TaxisListAdapter
import com.amp.pickup.viewmodel.TaxisListViewModel
import kotlinx.android.synthetic.main.fragment_taxis_list.*

/**
 * A simple [Fragment] subclass.
 *
 */
class TaxisListFragment : Fragment() {
    lateinit var binding: FragmentTaxisListBinding
    private val taxisViewModel by lazy { ViewModelProviders.of(this).get(TaxisListViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_taxis_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = context?.let { TaxisListAdapter(it) }
        adapter?.setFragment(this)
        driverRecyclerView.layoutManager = LinearLayoutManager(context)
        driverRecyclerView.adapter = adapter

        taxisViewModel.taxisList.observe(this, Observer { taxis ->
            taxis?.let {
                adapter?.setTaxis(it)
            }
            print("advsdfghxbhdfg")
            println(taxis)
        })
        if (adapter != null) {
            println(adapter.taxisList)
        }
    }

}
