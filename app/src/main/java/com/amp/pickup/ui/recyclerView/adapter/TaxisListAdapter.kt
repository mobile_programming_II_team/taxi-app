package com.amp.pickup.ui.recyclerView.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.amp.pickup.R
import com.amp.pickup.data.model.Taxi
import com.amp.pickup.databinding.TaxisListItemBinding
import com.amp.pickup.viewmodel.TaxiViewModel

class TaxisListAdapter(val context: Context) :
    RecyclerView.Adapter<TaxisListAdapter.TaxiViewHolder>() {

    private val taxisViewModel by lazy { ViewModelProviders.of(frag).get(TaxiViewModel::class.java) }
    var taxisList: List<Taxi> = emptyList()
    private lateinit var frag: Fragment
    private lateinit var binding: TaxisListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaxiViewHolder {

        binding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.taxis_list_item, parent, false)
        print("thadhfbaskdjgbnwrgjlkhjs : ")
        println(taxisList)
        binding.viewmodel = taxisViewModel
        return TaxiViewHolder(binding)
    }

    override fun getItemCount(): Int = taxisList.size

    override fun onBindViewHolder(holder: TaxiViewHolder, position: Int) {
        val taxi = taxisList[position]
        holder.taxiBinding.viewmodel?.setTaxi(taxi)
    }

    internal fun setTaxis(taxis: List<Taxi>) {
        this.taxisList =
            listOf(
                Taxi(
                    id = "018bc45d-1627-4094-b4da-743aef4e8ac4",
                    vehicleType = "Corrolla Dx",
                    vehicleGrade = "AVERAGE",
                    driverId = "caad9e96-2f5d-4448-ad20-f241f132c47e"
                )
            )
        notifyDataSetChanged()
    }

    internal fun setFragment(fragment: Fragment) {
        this.frag = fragment
        notifyDataSetChanged()
    }

    class TaxiViewHolder(itemView: TaxisListItemBinding) : RecyclerView.ViewHolder(itemView.root) {
        var taxiBinding: TaxisListItemBinding = itemView

    }
}