package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentNavBarBinding
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_nav_bar.*

/**
 * A simple [Fragment] subclass.
 *
 */
class NavBarFragment : Fragment() {
    lateinit var binding: FragmentNavBarBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_nav_bar, container, false)

        (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.content_frame_home, MapFragmentOne()).addToBackStack(null).commit()

        return binding.root
    }


    private val onBottomNavigationIt = BottomNavigationView.OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.mapBottm -> {
                (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame_home, MapFragmentOne()).addToBackStack(null).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.driverBottm -> {
                (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame_home, TaxisListFragment()).addToBackStack(null).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.profile -> {
                (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
                    .replace(R.id.content_frame_home, UserStatus()).addToBackStack(null).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bottonNavHome.setOnNavigationItemSelectedListener(onBottomNavigationIt)

    }

}
