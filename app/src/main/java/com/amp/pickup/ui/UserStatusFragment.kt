package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentUserStatusBinding
import com.amp.pickup.viewmodel.UserStatusViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class UserStatus : Fragment() {
    lateinit var binding: FragmentUserStatusBinding
    private val userStatusViewModel by lazy { ViewModelProviders.of(this).get(UserStatusViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_user_status, container, false)
        binding.viewmodel = userStatusViewModel
        return binding.root
    }
}
