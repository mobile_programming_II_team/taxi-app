package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentRequestPageBinding
import com.amp.pickup.viewmodel.TaxiViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class RequestPage : Fragment() {
    lateinit var binding: FragmentRequestPageBinding
    private val taxiViewModel by lazy { ViewModelProviders.of(this).get(TaxiViewModel::class.java) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding.viewmodel = taxiViewModel
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_request_page, container, false)
        return binding.root
    }

}
