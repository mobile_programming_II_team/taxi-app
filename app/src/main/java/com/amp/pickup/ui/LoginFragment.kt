package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentLoginBinding
import com.amp.pickup.viewmodel.LoginViewModel

/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding
    private val loginViewModel by lazy { ViewModelProviders.of(this).get(LoginViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel.fragmentManager = (activity as AppCompatActivity).supportFragmentManager
//        loginViewModel.navController = Navigation.findNavController((activity as AppCompatActivity), R.id.my_nav_host_fragment)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_login, container, false)
        binding.viewmodel = loginViewModel
        return binding.root
    }
}
