package com.amp.pickup.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.amp.pickup.R
import com.amp.pickup.databinding.FragmentFirstPageBinding

/**
 * A simple [Fragment] subclass.
 *
 */
class FirstFragmentPage : Fragment() {
    lateinit var binding: FragmentFirstPageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_first_page, container, false)
        return binding.root
    }

}
