package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Feedback
import com.amp.pickup.data.model.Location
import com.amp.pickup.data.model.User
import com.amp.pickup.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class UserViewModel(application: Application) : AndroidViewModel(application) {

    private val userRepository: UserRepository
    var userById: LiveData<User>? = null
    var userFeedbacks: LiveData<List<Feedback>>? = null

    init {
        val userDao = CacheDatabase.getDatabase(application).userDao()
        val feedbackDao = CacheDatabase.getDatabase(application).feedbackDao()
        userRepository = UserRepository(userDao, application, feedbackDao)

    }

    fun findUserById(authHeader: String, id: String) = viewModelScope.launch(Dispatchers.IO) {
        userById = userRepository.findUserById(id)
    }

    fun findUserFeedbacks(authHeader: String, userId: String) = viewModelScope.launch(Dispatchers.IO) {
        userFeedbacks = userRepository.findUserFeedbacks(userId)
    }

    fun requestToBeDriver(authHeader: String, vehicleType: String, vehicleGrade: String) =
        viewModelScope.launch(Dispatchers.IO) {
            userRepository.requestToBeDriver(vehicleType, vehicleGrade)
    }


    fun approvedDriver(authHeader: String) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.approveDriver()
    }

    fun denyedDriver(authHeader: String) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.deniedDriver()
    }

    fun addFeedback(authHeader: String, userId: String, newFeedback: Feedback) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.addFeedback(userId, newFeedback)
    }

    fun updateLocation(authHeader: String, newLocation: Location) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.updateLocation(newLocation)
    }

    fun editAccount(authHeader: String, newUser: User) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.updateUser(newUser)
    }

    fun insertUser(user: User) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.insertUser(user)
    }

    fun updateUser(user: User) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.updateUser(user)
    }

    fun deleteUser(user: User) = viewModelScope.launch(Dispatchers.IO) {
        userRepository.deleteUser(user)
    }

}