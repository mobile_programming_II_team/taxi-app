package com.amp.pickup.viewmodel

import android.app.Application
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.R
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.User
import com.amp.pickup.repository.UserRepository
import com.amp.pickup.ui.LoginFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class RegisterViewModel(application: Application) : AndroidViewModel(application) {
    private var userRepository: UserRepository

    val name = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val confirmPassword = MutableLiveData<String>()
    val phoneNo = MutableLiveData<String>()

    val hasError = MutableLiveData<Boolean>()
    lateinit var fragmentManager: FragmentManager
    private val loginFragment = LoginFragment()

    var registeredUser: LiveData<User> = MutableLiveData()

    init {
        val database = CacheDatabase.getDatabase(application)
        val feedbackDao = database.feedbackDao()
        val fareDao = database.userDao()

        userRepository = UserRepository(fareDao, application, feedbackDao)
    }

    fun onRegister() = viewModelScope.launch(Dispatchers.IO) {
        println("register clicked")
        if (password.value != confirmPassword.value) {
            println("password mismatch")
            hasError.value = true
        } else {
            try {
                val user = User(
                    UUID.randomUUID().toString(),
                    name.value,
                    email.value,
                    password.value,
                    phoneNo.value,
                    "NOT_DRIVER",
                    null,
                    "USER"
                )
                println(user)
                registeredUser = userRepository.insertUser(newUser = user)
                if (registeredUser.value == null) {
                    hasError.value = true
                }
                println("success")
                fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, loginFragment).addToBackStack(null).commit()
            } catch (exception: Exception) {
                println("Error saving user")
                hasError.value = true
            }
        }
    }
}