package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Feedback
import com.amp.pickup.data.model.Taxi
import com.amp.pickup.repository.TaxiRepository
import com.amp.pickup.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TaxiViewModel(application: Application) : AndroidViewModel(application) {

    private val taxiRepository: TaxiRepository
    private val userRepository: UserRepository

    val selectedTaxi = MutableLiveData<Taxi>()
    val driverName = MutableLiveData<String>()
    val rating = MutableLiveData<Double>()

    init {
        val database = CacheDatabase.getDatabase(application)
        val taxiDao = database.taxiDao()
        val feedbackDao = database.feedbackDao()
        val fareDao = database.userDao()

        taxiRepository = TaxiRepository(taxiDao, application)
        userRepository = UserRepository(fareDao, application, feedbackDao)
    }

    fun setTaxi(taxi: Taxi) {
        selectedTaxi.value = taxi
        driverName.value = userRepository.findUserById(taxi.driverId).value?.name
        rating.value = userRepository.findUserFeedbacks(taxi.driverId).value?.let { calculateRating(it) }
    }

    private fun calculateRating(feedbacks: List<Feedback>): Double {
        var total = 0.0
        for (feedback in feedbacks) {
            total += feedback.rating
        }
        return total / feedbacks.count()
    }

    fun updateTaxi(taxi: Taxi) = viewModelScope.launch(Dispatchers.IO) {
        taxiRepository.updateTaxi(taxi)
    }

    fun deleteTaxi(taxi: Taxi) = viewModelScope.launch(Dispatchers.IO) {
        taxiRepository.deleteTaxi(taxi)
    }

}