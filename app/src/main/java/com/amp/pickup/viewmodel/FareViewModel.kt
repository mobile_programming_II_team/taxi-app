package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Fare
import com.amp.pickup.data.model.Path
import com.amp.pickup.repository.FareRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FareViewModel(application: Application) : AndroidViewModel(application) {

    private val fareRepository: FareRepository
    var fare: LiveData<Fare>? = null

    var driverName = MutableLiveData<String>()
    var driverEmail = MutableLiveData<String>()
    var driverPhone = MutableLiveData<String>()
    var status = MutableLiveData<String>()

    init {
        val fareDao = CacheDatabase.getDatabase(application).fareDao()
        fareRepository = FareRepository(fareDao, application)
    }


//    fun findFareByPassengerId(passengerId: String): LiveData<List<Fare>> =
//        fareRepository.findFaresByPassengerId(passengerId)

    //fun findFareByTaxiId(taxiId: String): LiveData<List<Fare>> = fareRepository.findFaresByTaxiId(taxiId)

    fun findFareById(authHeader: String, id: String) = viewModelScope.launch(Dispatchers.IO) {
        fare = fareRepository.findFareById(id)
    }

    fun requestFare(authHeader: String, path: Path, taxiId: String) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.requestFare(path, taxiId)
    }

    fun changeStatus(authHeader: String, fareId: String, status: String) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.changeStatus(fareId, status)
    }


//    fun insertFare(authHeader: String , fare: Fare) = viewModelScope.launch(Dispatchers.IO) {
//        fareRepository.insertFare(authHeader,fare)
//    }

    fun updateFare(fare: Fare) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.updateFare(fare)
    }

    fun deleteFare(fare: Fare) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.deleteFare(fare)
    }


}