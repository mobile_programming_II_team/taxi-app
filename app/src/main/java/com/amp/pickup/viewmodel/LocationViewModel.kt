package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Location
import com.amp.pickup.repository.LocationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LocationViewModel(application: Application) : AndroidViewModel(application) {

    private val locationRepository: LocationRepository

    init {
        val locationDao = CacheDatabase.getDatabase(application).locationDao()
        locationRepository = LocationRepository(locationDao)
    }

    fun findLocationById(id: String): LiveData<Location> = locationRepository.findLocationById(id)

    fun insertLocation(location: Location) = viewModelScope.launch(Dispatchers.IO) {
        locationRepository.insertLocation(location)
    }

    fun updateLocation(location: Location) = viewModelScope.launch(Dispatchers.IO) {
        locationRepository.updateLocation(location)
    }

    fun deleteLocation(location: Location) = viewModelScope.launch(Dispatchers.IO) {
        locationRepository.deleteLocation(location)
    }
}