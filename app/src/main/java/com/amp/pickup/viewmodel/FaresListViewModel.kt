package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Fare
import com.amp.pickup.data.model.Path
import com.amp.pickup.repository.FareRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FaresListViewModel(application: Application) : AndroidViewModel(application) {

    private val fareRepository: FareRepository
    var allFares: LiveData<List<Fare>>? = null


    init {
        val fareDao = CacheDatabase.getDatabase(application).fareDao()
        fareRepository = FareRepository(fareDao, application)
    }


    fun findAllFare(authHeader: String, id: String) = viewModelScope.launch(Dispatchers.IO) {
        allFares = fareRepository.findAllFares()
    }

    fun requestFare(authHeader: String, path: Path, taxiId: String) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.requestFare(path, taxiId)
    }

    fun changeStatus(authHeader: String, fareId: String, status: String) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.changeStatus(fareId, status)
    }

    fun updateFare(fare: Fare) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.updareFare(fare)
    }

    fun deleteFare(fare: Fare) = viewModelScope.launch(Dispatchers.IO) {
        fareRepository.deleteFare(fare)
    }

}