package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Fare
import com.amp.pickup.data.model.Taxi
import com.amp.pickup.data.model.User
import com.amp.pickup.repository.FareRepository
import com.amp.pickup.repository.TaxiRepository
import com.amp.pickup.repository.UserRepository
import java.util.*

class UserStatusViewModel(application: Application) : AndroidViewModel(application) {

    private var userRepository: UserRepository
    private var taxiRepository: TaxiRepository
    private var fareRepository: FareRepository

    var fare: LiveData<Fare> = MutableLiveData()
    var taxi: LiveData<Taxi> = MutableLiveData()
    var passenger: LiveData<User> = MutableLiveData()
    var driver: LiveData<User> = MutableLiveData()

    init {
        val database = CacheDatabase.getDatabase(application)
        val feedbackDao = database.feedbackDao()
        val userDao = database.userDao()
        val taxiDao = database.taxiDao()
        val fareDao = database.fareDao()

        userRepository = UserRepository(userDao, application, feedbackDao)
        taxiRepository = TaxiRepository(taxiDao, application)
        fareRepository = FareRepository(fareDao, application)

        fare = fareRepository.findFareById(UUID.randomUUID().toString())
//        taxi = taxiRepository.findTaxiById(fare.value!!.taxiId)
//        driver = userRepository.findUserById(taxi.value!!.driverId)
//        driver = userRepository.findUserById(fare.value!!.passengerId)
    }
}