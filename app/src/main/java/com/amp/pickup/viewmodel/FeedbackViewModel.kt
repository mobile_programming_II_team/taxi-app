package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Feedback
import com.amp.pickup.repository.FeedbackRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FeedbackViewModel(application: Application) : AndroidViewModel(application) {

    private val feedbackRepository: FeedbackRepository

    init {
        val feedbackDao = CacheDatabase.getDatabase(application).feedbackDao()
        feedbackRepository = FeedbackRepository(feedbackDao)
    }

    fun findFeedBackByUserId(userId: String): LiveData<List<Feedback>> = feedbackRepository.findFeedbackByuserId(userId)

    fun findFeedbackById(id: String): LiveData<Feedback> = feedbackRepository.findFeedbackById(id)

    fun insertFeedback(feedback: Feedback) = viewModelScope.launch(Dispatchers.IO) {
        feedbackRepository.insertFeedback(feedback)
    }

    fun updateFeedback(feedback: Feedback) = viewModelScope.launch(Dispatchers.IO) {
        feedbackRepository.updateFeedBack(feedback)
    }

    fun deleteFeedback(feedback: Feedback) = viewModelScope.launch(Dispatchers.IO) {
        feedbackRepository.deleteFeedback(feedback)
    }

}
