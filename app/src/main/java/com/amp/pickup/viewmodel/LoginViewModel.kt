package com.amp.pickup.viewmodel

import android.app.Application
import android.content.Context
import android.util.Base64
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.amp.pickup.R
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.User
import com.amp.pickup.repository.UserRepository
import com.amp.pickup.ui.NavBarFragment
import kotlinx.coroutines.launch

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    private var userRepository: UserRepository
    private val simpleSharedPreferences =
        application.getSharedPreferences(application.resources.getString(R.string.shared_pref), Context.MODE_PRIVATE)
    private val dataKey = application.resources.getString(R.string.auth_header)
    private val emailKey = application.resources.getString(R.string.logged_in_email)

    val emailText = MutableLiveData<String>()
    val passwordText = MutableLiveData<String>()
    val hasError = MutableLiveData<Boolean>()

    private val loggedIn = MutableLiveData<User>()

    lateinit var fragmentManager: FragmentManager
    private val navFragment = NavBarFragment()
    val navController: NavController? = null

    init {
        val database = CacheDatabase.getDatabase(application)
        val feedbackDao = database.feedbackDao()
        val fareDao = database.userDao()

        userRepository = UserRepository(fareDao, application, feedbackDao)
    }

    fun onLogin() = viewModelScope.launch {
        val base = emailText.value + ":" + passwordText.value

        simpleSharedPreferences?.edit()?.clear()?.apply()
        val authHeader = "Basic " + Base64.encodeToString(base.toByteArray(), Base64.NO_WRAP)

        simpleSharedPreferences?.edit()?.putString(dataKey, authHeader)?.putString(emailKey, emailText.value)?.apply()

        userRepository.findLoggedInUser().observeForever { loggedInUser ->
            print("Finally: ")
            println(loggedInUser)
            loggedIn.value = loggedInUser

            if (loggedIn.value != null) {
                fragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, navFragment).addToBackStack(null).commit()
            } else {
                hasError.value = true
            }
        }

    }
}