package com.amp.pickup.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Taxi
import com.amp.pickup.repository.TaxiRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class TaxisListViewModel(application: Application) : AndroidViewModel(application) {

    private var taxiRepository: TaxiRepository
    var taxisList: LiveData<List<Taxi>> = MutableLiveData()

    init {
        val database = CacheDatabase.getDatabase(application)
        val taxiDao = database.taxiDao()
        taxiRepository = TaxiRepository(taxiDao, application)

        runBlocking { findAllTaxis() }

    }

    suspend fun findAllTaxis() = viewModelScope.launch {
        taxisList = taxiRepository.findAllTaxis()
    }
}