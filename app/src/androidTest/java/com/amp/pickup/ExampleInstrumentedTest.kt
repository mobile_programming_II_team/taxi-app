package com.amp.pickup

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.amp.pickup", appContext.packageName)
    }

//    @Test
//    fun clickAddTaskButton_navigateToAddEditFragment() {
//        val scenario = launchFragmentInContainer<LoginFragment>(Bundle(), R.style.AppTheme)
//        val navController = mock(NavController::class.java)
//
//        scenario.onFragment {
//            Navigation.setViewNavController(it.view!!, navController)
//        }
//
//        // When the FAB is clicked
//        onView(withId(R.id.signIn)).perform(click())
//
//        // Then verify that we navigate to the add screen
//        verify(navController).navigate(
//            LoginFragmentDirections.actionFragmentLoginToNavBarFragment()
//        )
//
//    }
}
