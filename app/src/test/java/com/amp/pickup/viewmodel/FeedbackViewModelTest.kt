package com.amp.pickup.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Feedback
import com.amp.pickup.repository.FeedbackRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class FeedbackViewModelTest {

    private lateinit var feedbackViewModel: FeedbackViewModel
    private lateinit var cacheDatabase: CacheDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        cacheDatabase = Room.inMemoryDatabaseBuilder(context, CacheDatabase::class.java).build()

        val feedbackRepository = FeedbackRepository(cacheDatabase.feedbackDao())
        feedbackViewModel = FeedbackViewModel(Application())
    }

    @After
    fun tearDown() {
        cacheDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun findFeedBackByUserId() {
        val feedback = Feedback("1", "2", 4, "not bad")
        feedbackViewModel.insertFeedback(feedback)

        val result = feedbackViewModel.findFeedBackByUserId("2") as Feedback
        assertEquals(result.id, feedback.id)
    }

    @Test
    @Throws(InterruptedException::class)
    fun findFeedbackById() {
        val feedback = Feedback("1", "2", 4, "not bad")
        feedbackViewModel.insertFeedback(feedback)

        val result = feedbackViewModel.findFeedbackById("1") as Feedback
        assertEquals(result.id, feedback.id)
    }

    @Test
    @Throws(InterruptedException::class)
    fun updateFeedback() {
        var feedback = Feedback("1", "2", 4, "not bad")
        feedbackViewModel.insertFeedback(feedback)

        feedback = Feedback("1", "2", 4, "good")
        feedbackViewModel.updateFeedback(feedback)

        val result = feedbackViewModel.findFeedbackById("1") as Feedback
        assertEquals(result.review, "good")

    }

    @Test
    @Throws(InterruptedException::class)
    fun deleteFeedback() {
        var feedback = Feedback("1", "2", 4, "not bad")
        feedbackViewModel.insertFeedback(feedback)

        feedbackViewModel.deleteFeedback(feedback)
        val result = feedbackViewModel.findFeedbackById("1")
        assertEquals(result, null)

    }
}