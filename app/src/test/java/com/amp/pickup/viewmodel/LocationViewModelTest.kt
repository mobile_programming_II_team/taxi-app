package com.amp.pickup.viewmodel

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.amp.pickup.data.CacheDatabase
import com.amp.pickup.data.model.Location
import com.amp.pickup.repository.LocationRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LocationViewModelTest {

    private lateinit var locationViewModel: LocationViewModel
    private lateinit var cacheDatabase: CacheDatabase

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        cacheDatabase = Room.inMemoryDatabaseBuilder(context, CacheDatabase::class.java).build()

        val locationRepository = LocationRepository(cacheDatabase.locationDao())
        locationViewModel = LocationViewModel(Application())

    }

    @After
    fun tearDown() {
        cacheDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun findLocationById() {
        val location = Location("1", 12.12, 13.13)
        locationViewModel.insertLocation(location)

        val result = locationViewModel.findLocationById("1") as Location
        assertEquals(result.id, location.id)
    }

    @Test
    @Throws(InterruptedException::class)
    fun updateLocation() {
        var location = Location("1", 12.12, 13.13)
        locationViewModel.insertLocation(location)

        location = Location("1", 15.23, 14.34)
        locationViewModel.updateLocation(location)

        val result = locationViewModel.findLocationById("1") as Location
        assertEquals(result.latitude, 15.23)

    }

    @Test
    @Throws(InterruptedException::class)
    fun deleteLocation() {
    }
}